#!/usr/bin/env python3
import json
#import time
#replaced by the following two lines for testing purposes
import falsifytime
time = falsifytime.falsifytime()

#for generation of messages
from random import random
from hashlib import sha256
# on importe les libraires et fonction necessaires 

class Message:  # On crée la classe message 
	#some constants (message types)
	MSG_ISAID = 0 	#What-I-said message
	MSG_IHEARD = 1	#What-I-heard message
	MSG_THEY = 2	#What-covid-19-infected-said message    
    #On initialise 3 variables difféntes en fonction du type de message 
	#the constructor can take various parameters
	#default is parameterless, it generates a message to be said
	#if type is not specified, it is to be imported from an export string or parsed export string
	#otherwise, if date is False the time is system time
		#else it is a float, then the date should be this parameter
	def __init__(self, msg="", msg_type=False, msg_date=False):    #On crée la fonction init ayant 3 arguments, msg initialisé avec un texte vide "", msg_type initialisé à False et msg_date initialisé aussi à False
		if msg == "":    # Si lors de la création du message, aucun argument n'a été mentionné alors :  
			self.content = Message.generate()  #on genère un message grace à la fonction generate créer plus bas. 
			self.type = Message.MSG_ISAID   #On fixe le type de message à 0 (donc MSG_ISAID)
			self.date = time.time()   # on fixe la date à la date actuelle
		elif msg_type is False :    #Si nos avons un message mais pas de type de message comme argument, alors on importe le type de message grace à la fonction m_import créee en fin de code. 
			self.m_import(msg)
		else:    # Sinon, on a dans nos arguments un message et un type de message 
			self.content = msg    #on attribue le msg à self.content
			self.type = msg_type   #on attribue le msg_type à self.type
			if msg_date is False:   # si nous n'avons pas de date alors on fixe la date à la date actuelle 
				self.date = time.time()
			else:    # si nous avons une date, alors ona ttribue msg_date à self.date
				self.date = msg_date

	#a method to print the date in a human readable form
	def hdate(self):     # Cette fonction n'a pas d'attibuts, elle sert à convertir la date self.date en une chaine de caractère repésentant l'heure locale 
		return time.ctime(self.date)

	#a method to compute the age of a message in days or in seconds
	#for display purpose, set as_string to True
	def age(self, days=True, as_string=False): #on crée  la fonction age avec comme arguments days et as string. Cette fonction a pour but de retourner l'age d'un message. 
		age = time.time() - self.date   #on calcule l'age du message 
		if days:   #Si notre resultat est en jour, on le convertie en seconde
			age = int(age/(3600*24))
		if as_string:    # si c'est une chaine de caractère, on le convertit également en seconde à l'aide de division euclidiennes successives. 
			d = int(age/(3600*24))
			r = age%(3600*24)
			h = int(r/3600)
			r = r%3600
			m = int(r/60)
			s = r%60
			age = (str(age)+"~"+str(d)+"d"+ str(h) + "h"+str(m)+"m"+str(s)+"s")
		return age  # on retourne la valeur age que nous avons trouvée 

	#testers of message type
	def is_i_said(self):    
		return self.type == Message.MSG_ISAID
	def is_i_heard(self):
		return self.type == Message.MSG_IHEARD
	def is_they_said(self):
		return self.type == Message.MSG_THEY    # on crée 3 fonctions qui retournent le type de message 

	#setters
	def set_type(self, new_type):
		self.type = new_type    # cette fonction a comme attribut un nouveau type de message, elle sert à creer un nouveau type de message

	#a class method that generates a random message
	@classmethod
	def generate(cls):
		return sha256(bytes(str(time.time())+str(random()),'utf8')).hexdigest()   
# Cette méthode sert à générer un message aléatoire lorsque elle est appelée

	#a method to convert the object to string data
	def __str__(self):
		return f"\t\t<message type=\"{self.type}\">\n\t\t\t<content>{self.content}</content>\n\t\t\t<date>{self.hdate()}</date>\n\t\t</message>"
# Cette fonction convertie l'objet (self) en texte 

	#export/import
	def m_export(self):
		return f"{{\"content\":\"{self.content}\",\"type\":{self.type}, \"date\":{self.date}}}"
# Cette fonction exporte le contenu du message, le type ainsi que la date 


	def m_import(self, msg):    # Cette fonction importe sert à imoorter le message qu'elle a en argument
		if(type(msg) == type(str())):   # On traite différent le message si on le recoit sous format python ou json 
			json_object = json.loads(msg)
		elif(type(msg) == type(dict())):
			json_object = msg   # Dans les 2 cas on affecte à json object le message en format json 
		else:
			raise ValueError   #Si le message n'est dans aucun des 2 types, la fonction renvoie une erreur 
		self.content = json_object["content"]
		self.type = json_object["type"]
		self.date = json_object["date"]   # On affecte à au content, type et date les valeur contenue dans le message.

#will only execute if this file is run
if __name__ == "__main__":     # Cette méthode s'exécute seulement si on execute directement le fichier python. Cela permet de mettre du code de test qui ne soit pas exécuté lors d’un import.
	#test the class
	myMessage = Message()
	time.sleep(1)
	mySecondMessage = Message(Message.generate(), Message.MSG_THEY)
	copyOfM = Message(myMessage.m_export())
	print(myMessage)
	print(mySecondMessage)
	print(copyOfM)
	time.sleep(0.5)
	print(copyOfM.age(True,True))
	time.sleep(0.5)
	print(copyOfM.age(False,True))
