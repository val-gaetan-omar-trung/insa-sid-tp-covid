#!/usr/bin/env python3
from flask import Flask #server
from flask import request #to handle the different http requests
from flask import Response #to reply (we could use jsonify as well but we handled it)
from flask import render_template #to use external files for responses (out of the scope of the project, added to provide a test UI)
import json
#My libraries
from message import Message #wraps various standard library module, web app API calls etc.
from client import Client #embarks message_catalog

# on importe toutes les librairies necessaires à notre code. 
# Flask sert a créer un site web / json sert pour le stockage et l'échange de données 
# On importe également nos classes que nous avons créees. 

app = Flask(__name__)  # on crée une instance de Flask 

#“Routes” will handle all requests to a specific resource indicated in the
#@app.route() decorator

#Theses route are for the "person" use case
	#case 1 refuse GET
@app.route('/', methods=['GET'])    #On dit a notre application à quelle url notre fonction s'exécute 
#pour recevoir une représentation de la ressource
def index(): #pour générer HTML à partir de python
	response = render_template("menu.html",root=request.url_root, h="") #pour render un template, on fournit le nom du template et les variables que l'on veut passer au template sous forme d'arguments
	return response  # on crée la fonction index qui nous renvoit l'argument reponse. Cet argument contiendra le template appliqué à la page "menu.html"

	#case 2 hear message
@app.route('/<msg>', methods=['POST'])   #On dit a notre app à quelle url notre fonction s'exécute. Ce n'est pas au même endroit que précédement, c'est à la localisation  '/<msg>' 
def add_heard(msg): #la méthode est 'POST', on recoit donc la metadata de la ressource
	if client.catalog.add_message(Message(msg, Message.MSG_IHEARD)): #tester la possibilité de l'ajout d'un message, c'est-à-dire si un message a été 'écouté'
		response = Response(f"Message “{msg}” received.", mimetype='text/plain', status=201) #statut 201 : requete réussie et ressource créée
	else :
		reponse = Response(status=400) #Bad request : le serveur ne peut pas comprendre la requete en raison d'une synthaxe invalide. L'expressiont testée est donc fausse
	return response   # On crée la fonction add_heard qui retourne le message "écouté"
#End of person use case

#Hospital use case
@app.route('/they-said', methods=['GET','POST']) #ajout d'une nouvelle route d'accès à laquelle peuvent être appliquée les deux méthodes GET et POST
def hospital():
	if request.method == 'GET':
		#Wants the list of covid messages (they said)
		client.catalog.purge(Message.MSG_THEY) 
		response = Response(client.catalog.c_export_type(Message.MSG_THEY), mimetype='application/json', status=200) #recevoir la liste des messages 'they said'
	elif request.method == 'POST':
		if request.is_json: #vérifier le format des données
			req = json.loads(request.get_json()) #charger toute la liste des messages
			response = client.catalog.c_import_as_type(req, Message.MSG_THEY) #Importer le message et changer son type
			response = Response(f"{response} new “they said” messages.", mimetype='text/plain', status=201)
		else:
			response = Response(f"JSON expected", mimetype='text/plain', status=400) #Bad request : le serveur ne peut pas comprendre la requete en raison d'une synthaxe invalide. L'expression testée est donc fausse
	else:
		reponse = Response(f"forbidden method “{request.method}”.",status=403) #le serveur comprend la requete mais refuse de l'executer
	return response #selon si c'est un GET ou un POST, l'hopital recoit les message 'écouté' ou envoit une alerte aux téléphones ayant écouté suffisamment de messages de la personne concernée 
#End hospital use case
#NO NEED TO COMMENT
#Begin UI
#These routes are out of the scope of the project, they are here
#to provide a test interface
@app.route('/check/<host>', methods=['GET'])
def check(host):
	h = host.strip()
	n = client.get_covid(h)
	r = dict()
	r["text"] = f"{n} they said messages imported from {h} ({client.r.status_code})"
	if client.catalog.quarantine(4):
		r["summary"] = "Stay home, you are sick."
	else:
		r["summary"] = "Everything is fine, but stay home anyway."
	return render_template("menu.html",responses=[r],root=request.url_root, h=h)

@app.route('/declare/<host>', methods=['GET'])
def declare(host):
	h = host.strip()
	client.send_history(h)
	r=[{"summary":f"Declare covid to {h} ({client.r.status_code})",
	"text":client.r.text}]
	return render_template("menu.html",responses=r,root=request.url_root, h=h)

@app.route('/say/<hosts>', methods=['GET'])
def tell(hosts):
	hosts = hosts.split(",")
	r=[]
	for host in hosts:
		h = host.strip()
		client.say_something(h)
		r.append({"summary":f"ISAID to {h} ({client.r.status_code})",
		"text":client.r.text})
	return render_template("menu.html", responses=r, root=request.url_root, h=h)
#end UI

#will only execute if this file is run
if __name__ == "__main__":
	debugging = True
	client = Client("client.json", debugging)
	app.run(host="0.0.0.0", debug=debugging)
