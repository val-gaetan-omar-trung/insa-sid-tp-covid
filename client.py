#!/usr/bin/env python3
from message import Message
from message_catalog import MessageCatalog
import requests
# On importe les classes crées au préalable et le module requests 

class Client:   # On crée la classe client 
	#constructor, takes a message catalog
	def __init__(self, catalog_path, debug = False, defaultProtocol="http://"):  #Dans le constructeur de la classe on a 3 arguements 
		self.catalog = MessageCatalog(catalog_path)    #on attribue à catalogue le catalogue se trouvant au chemin d'accès catalogue_path
		if debug:   #Si le debug n'est pas nécessaire, on affiche le catalogue 
			print(self.catalog)
		self.r = None   
		self.debug = debug   #L'attribut debut prend la valeur debug de l'argument 
		self.protocol = defaultProtocol   #L'attribut protocol prend la valeur defaultprotocol de l'argument 

	@classmethod  
	def withProtocol(cls, host):    # La fonction withProtocol permet de retrouver le chemin d'accès à partir de l'host directement 
		res = host.find("://") > 1   #On cherche l'adresse de host qu'on affecte à res
		return res

	def completeUrl(self, host, route = ""):    #La fonction completeUrl permet de determiner l'url complete. 
		if not Client.withProtocol(host):   #Si l'on ne trouve pas l'host grâce à la fonction Client.withProtocol(host), on affecte à host la valeur self.protocol + host 
			host = self.protocol + host
		if route != "":   # Si la route en argument n'est pas vide, on ajoute un / devant pour pouvoir l'ajouter à l'host afin de créer l'url complete
			route = "/"+route
		return host+route   # l'ajout de host et route nous donne l'URL complete, c'est le résultat que l'on retourne

	#send an I said message to a host
	def say_something(self, host):      # Cette fonction permet d'envoyer un message au serveur lorsque l'on est contaminé 
		m = Message()   #On crée un objet message que l'on ajoute au catalogue 
		self.catalog.add_message(m)
		route = self.completeUrl(host, m.content)   #On affecte à rout l'url complete du message 
		self.r = requests.post(route)   #On fait la requete pour poster un message à l'url "route"
		if self.debug:  
			print("POST  "+route + "→" + str(self.r.status_code))    #On attribut au POST le statut du code 
			print(self.r.text)   #On affiche le resultat de la requete  
		return self.r.status_code == 201   #Si le status_code est de 201 (cela signifie que le message a bien été crée) alors on return True sinon False 

	#add to catalog all the covid from host server
	def get_covid(self, host):   # CEtte fonction permet d'ajouter à un catalogue tout les cas de covid présents sur un serveur 
		route = self.completeUrl(host,'/they-said')   #On affecte à route l'url complete du message avec comme attribut la branche they said 
		self.r = requests.get(route)       #On fait la requete pour avoir des données sur l'url "route"
		res = self.r.status_code == 200    #Le res est à True si le statut de la requete est 200 (requete ok) sinon il est à false 
		if res:
			res = self.catalog.c_import(self.r.json())   # Si res est a True, on importe le catalogue 
		if self.debug:
			print("GET  "+ route + "→" + str(self.r.status_code))    #On attribut au GET le statut du code de la requete
			if res != False:   # Si le resultat est True on affiche le catalogue 
				print(str(self.r.json()))
		return res

	#send to server list of I said messages
	def send_history(self, host):    #La fonction send_history permet d'envoyer au serveur tout les messages qu'un client a dit 
		route = self.completeUrl(host,'/they-said')   #On affecte à route l'url complete du message avec comme attribut la branche they said 
		self.catalog.purge(Message.MSG_ISAID)    #On purge le catalogue de message isaid 
		data = self.catalog.c_export_type(Message.MSG_ISAID)   #On exporte du catalogue les messages du type isaid 
		self.r = requests.post(route, json=data)   #On fait la requete pour post les données sur l'URL "route "
		if self.debug:
			print("POST  "+ route + "→" + str(self.r.status_code))  #On attribut au POST le statut du code de la requete
			print(str(data))  
			print(str(self.r.text))   # On affiche le résultat de la requete 
		return self.r.status_code == 201    #Cette fonction retourne True si on a pu créer les messages à l'url sinon elle retourne False 


if __name__ == "__main__":     # Cette méthode s'exécute seulement si on execute directement le fichier python. Cela permet de mettre du code de test qui ne soit pas exécuté lors d’un import.
	c = Client("client.json", True)
	c.say_something("localhost:5000")
	c.get_covid("localhost:5000")
	c.send_history("localhost:5000")
