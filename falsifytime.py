#!/usr/bin/env python3
def falsifytime():   # On cr�e la fonction falsify time afin de truquer la date pour la faire avancer de 14 jours toutes les minutes 
	import time as time   # On importe le module n�cessaire
	#the referencetime is when we load this module
	referencetime = time.time()    #on configure le temps de r�f�rence au moment ou l'on appelle cette fonction 
	#factor is how many seconds of fake time correspond to a real second
	factor = 3600*24*14/60 #one real minute is fourteen days of fake time

	#override time.time()
	real_time = time.time
	def fake_time():   # La fonction fake time calcule la dur�e qui s'est �coul�e et lui applique le facteur de 1 minute pour 14 jours 
		duration = real_time() - referencetime  
		return referencetime + factor*duration
	time.time = fake_time   #On attribue donc � la variable time, le temps truqu� � l'aide du facteur d�fini. 
	return time     # on retourne comme r�sultat ce fake_time 
