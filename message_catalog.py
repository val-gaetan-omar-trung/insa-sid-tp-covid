#!/usr/bin/env python3
import json, os
from message import *
#On importe les modules necessaires à notre code ainsi que la totalité de la classe message

class MessageCatalog:    # On crée la classe messagecatalogue dans laquelle on stockera tout nos messages au format json
	#constructor, loads the messages (if there are any)
	#in this implementation we store all messages in a json file
	def __init__(self, filepath):   # Le constructeur de la classe avec comme arguement le chemin d'accès du fichier 
		#            I_SAID,I_HEARD,THEY
		self.data = [  []  ,  []   , [] ]    # On affecte aux données un tableau vide de 3 éléments 
		self.filepath = filepath   #Un des attributs de cette classe et les filepath auquel on affecte l'argument du constructeur 
		if os.path.exists(self.filepath):   #Cette méthode du module os permet de vérifier si le chemin d'acces à notre fichier existe déja ou pas 
			self.open()    # Si c'est le cas alors on l'ouvre 
		else:
			self.save()   #Sinon on l'enregistre 

	#destructor closes file.
	def __del__(self):     
		del self.filepath
		del self.data       # Cette fonction sert à supprimer un message du catalogue message 
        
        
	#get catalog size
	def get_size(self, msg_type=False):    # Cette fonction avec un argument msgtype étant par défaut à false sert à trouver la taille de notre message 
		if(msg_type is False):      #Si lorsque l'on appelle la fonction, il n'y a pas d'arguments alors on affecte au resultat la longueur des messages en sommant la longueur de tout les type de messages   
			res = self.get_size(Message.MSG_ISAID) + self.get_size(Message.MSG_IHEARD) + self.get_size(Message.MSG_THEY)
		else:
			res = len(self.data[msg_type])   # Si le type est présent dans l'argument, on affecte sa longueur au résultat 
		return res

	#Import object content     
	def c_import(self, content, save=True):      #On crée la fonction pour importer un objet catalogue 
		i = 0
		for msg in content:          #On parcourt les messages dans ce catalogue 
			#we don't save after adding message because we just
			#read the content
				if self.add_message(Message(msg), False):   #Si on peut ajouter le message, on l'ajoute et on incrémente le conteur de message ajouté
					i=i+1
		if save:
			self.save()   # Pour tout les messages ou le save est à True, on les sauvergarde au format json grace à la fonction save définie plus bas 
		return i

	#Import a set of messages but changing their type to new_type
	def c_import_as_type(self, content, new_type=Message.MSG_THEY):    #On importe un catalogue de message en modifiant leur type en un nouveau type qui est par defaut à messagethey
		i=0
		for msg in content:     #Pour les messages dans ce catalogue 
			m = Message(msg)
			m.set_type(new_type)   #On redéfinie le type de message à un nouveau type 
			if self.add_message(m, False):   #Si on peut ajouter le message alors on le fait et on incrémente le nombre de message ajouté 
				i=i+1
		if i > 0:    #On sauvegarde les nouveaux messages grace a la fonction save défini en fin de code  
			self.save()
		return i   #On retourne le nombre de nouveau message sauvegardé

	#Open = load from file
	def open(self):    # On définie une nouvelle fonction open qui permet d'ouvrir le fichier en fonction de son chemin d'accès 
		file = open(self.filepath,"r")
		self.c_import(json.load(file), False)   #On importe toutes les données et on referme le lien 
		file.close()

	#export the messages of a given type from a catalog
	def c_export_type(self, msg_type, indent=2, bracket=True):       #Cette nouvelle fonction exporte les messages d'un certain type d'un catalogue 
		tmp = int(bracket) * "[\n"
		first_item = True
		for msg in self.data[msg_type]:     #Pour tout les messages de ce type 
			if first_item:
				first_item=False    #Si c'est le premier message que l'on traite on change la variable first itm a false. 
			else:
				tmp = tmp +",\n"    # si ce n'est pas le premier message, on saute une ligne avant de l'écrire
			tmp = tmp + (indent*" ") +  msg.m_export()   #On écrit le message en indentant dans la variable tmp
		tmp = tmp + int(bracket) * "\n]"
		return tmp

	#Export the whole catalog under a text format
	def c_export(self, indent=2):    # Cette fonction exporte tout le catalogue au format texte 
		tmp = ""   #Cette variable va servir a écrire le catalogue au format texte 
		if(len(self.data[Message.MSG_ISAID])> 0 ):
			tmp = tmp + self.c_export_type(Message.MSG_ISAID, indent, False)  #On écrit premièrement tout les messages du type "isaid"
		if(len(self.data[Message.MSG_IHEARD]) > 0):
			if tmp != "":
				tmp = tmp + ",\n"     #Si tmp n'est pas vide il nous faut sauter une ligne a avant de continuer a écrire la suite du catalogue 
			tmp = tmp + self.c_export_type(Message.MSG_IHEARD, indent, False)   # On écrit maintenant tout les messages de type "iheard"
		if(len(self.data[Message.MSG_THEY]) > 0):
			if tmp != "":
				tmp = tmp + ",\n"   #Si tmp n'est pas vide il nous faut sauter une ligne a avant de continuer a écrire la suite du catalogue
			tmp = tmp + self.c_export_type(Message.MSG_THEY, indent, False)    # On écrit maintenant tout les messages de type "they"
		tmp = "[" + tmp + "\n]"    # On met la chaine de caractère entre crochets avant de l'exporter
		return tmp

	#a method to convert the object to string data
	def __str__(self):    # Cette méthode permet de convertir l'objet en texte 
		tmp="<catalog>\n"    # première ligne du texte, on ouvre la balise catalogue
		tmp=tmp+"\t<isaid>\n"       # après avoir sauté une ligne on écrit le titre isaid   
		for msg in self.data[Message.MSG_ISAID]:
			tmp = tmp+str(msg)+"\n"    #On écrit ensuite tous les messages de type isaid
		tmp=tmp+"\n\t</isaid>\n\t<iheard>\n"   # après avoir sauté une ligne on écrit le titre iheard
		for msg in self.data[Message.MSG_IHEARD]:   
			tmp = tmp+str(msg)+"\n"   #On écrit ensuite tous les messages de type iheard
		tmp=tmp+"\n\t</iheard>\n\t<theysaid>\n"   # après avoir sauté une ligne on écrit le titre theysaid
		for msg in self.data[Message.MSG_THEY]:  
			tmp = tmp+str(msg)+"\n"    #On écrit ensuite tous les messages de type theysaid
		tmp=tmp+"\n\t</theysaid>\n</catalog>"   #On ferme le catalogue 
		return tmp

	#Save object content to file
	def save(self):    # On crée la fonction save ayant pour but de sauvegarder le fichier que l'on ouvre. 
		file = open(self.filepath,"w")    #On ouvre le fichier en suivant son chemin d'accès
		file.write(self.c_export())   #On l'écrit (=sauvegarde)
		file.close()    # On ferme l'accès 
		return True   #On retourne que la sauvergarde a bien été réalisée 

	#add a Message object to the catalog
	def add_message(self, m, save=True):    #On crée la fonction addmessage qui permet d'ajouter un message au catalogue si il n'est pas déja présent 
		res = True
		if(self.check_msg(m.content, m.type)):    #Si le message est déjà présent, on renvoit un texte le spécifiant 
			print(f"{m.content} is already there")
			res = False
		else:    #Si il n'est pas encore présent, on l'ajoute aux messages du meme type
			self.data[m.type].append(m)
			if save:
				res = self.save()   #Si il faut le sauvegarder alors on le fait 
		return res    #la fonction retourne false si le message n'a pas été ajouté (il etait déja présent) et true si il a bien été ajouté 

	#remove all messages of msg_type that are older than max_age days
	def purge(self, max_age=14, msg_type=False):       # Cette fonction purge sert a supprimé les messages datant de plus de 14 jours 
		if(msg_type is False):     
			self.purge(max_age, Message.MSG_ISAID)   # Si lors de l'appel de cette fonction il n'y a pas d'arguments, on refait appel à la même foncton 3 fois en purgeant tout les messages des 3 différents types  
			self.purge(max_age, Message.MSG_IHEARD)   
			self.purge(max_age, Message.MSG_THEY)
		else:    # Lorsque il y a bien l'argument msg_type
			removable = []    # On crée une liste dans laquelle on va append tout les messages que nous pourrons supprimer 
			for i in range(len(self.data[msg_type])):      # On parcourt les messages du type msg_type
				if(self.data[msg_type][i].age(True)>max_age):   
					removable.append(i)   #Si l'age du message est plus grand que le max age alors on l'ajoute à la liste des éléments a supprimer 
			while len(removable) > 0:
					del self.data[msg_type][removable.pop()]   #On supprime la totalité de la liste de message à supprimer
			self.save()  # On sauvergarde le catalogue une fois ce dernier purgé
		return True   # Lorsque l'opération a été effectuée, on retourne True 

	#Check if message string is in a category
	def check_msg(self, msg_str, msg_type=Message.MSG_THEY):  #On crée la fonction check_msg pour savoir si le message en argument de la fonction est déja présent dans la db 
		for msg in self.data[msg_type]:   #On parcourt la liste de message ayant le meme type que celui en argument de la fonction 
			if msg_str == msg.content:
				return True   #Sion trouve un message ayant le meme contenu, alors on retourne True ( ca veut dire que le message est déja présent )
		return False # sinon le message n'est pas encore présent, on retourne False 

	#Say if I should quarantine based on the state of my catalog
	def quarantine(self, max_heard=4):    #La fonction quarantine sert à savoir si il faut se confiner en fonction du nombre de personnes contaminée avec lesquelles nous avons été en contact
		self.purge()    # Dans un premier temps on supprime les messages datant de plus de 14 jours 
		n = 0    # Cette variable va servir à savoir avec combien de personnes nous avons été en contact 
		for msg in self.data[Message.MSG_IHEARD]:  
			if self.check_msg(msg.content):   #Pour chaques messages entendu, on check si l'on a été en contact avec. 
				n = n + 1    # Si c'est le cas alors on incrémente n 
		print(f"{n} covid messages heard")    # On écrit le nombre total de contact 
		return max_heard < n 

#will only execute if this file is run
if __name__ == "__main__":      # Cette méthode s'exécute seulement si on execute directement le fichier python. Cela permet de mettre du code de test qui ne soit pas exécuté lors d’un import.
	#test the class
	#test the class
	catalog = MessageCatalog("test.json")
	catalog.add_message(Message())
	catalog.add_message(Message(Message.generate(), Message.MSG_IHEARD))
	print(catalog.c_export())
	time.sleep(0.5)
	catalog.add_message(Message(f"{{\"content\":\"{Message.generate()}\",\"type\":{Message.MSG_THEY}, \"date\":{time.time()}}}"))
	catalog.add_message(Message())
	print(catalog.c_export())
	time.sleep(0.5)
	catalog.add_message(Message())
	print(catalog.c_export())
	time.sleep(2)
	catalog.purge(2)
	print(catalog)
